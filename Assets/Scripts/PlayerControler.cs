using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;

public class PlayerControler : NetworkBehaviour
{
    public float moveSpeed = 5f;
    Animator animator;

    float x;
    float z;
    bool isGrounded;
    float groundDistance = 1f;
    bool canJump;

    float turnSmoothVelocity;

    [SerializeField] float jumpForce = 10f;
    [SerializeField] LayerMask layerGround;

    Vector3 moveDirection;

    Rigidbody rb;
    public Transform cam;

    private void Start()
    {
        if (IsLocalPlayer)
        {
            animator = GetComponentInChildren<Animator>();
            Cursor.lockState = CursorLockMode.Locked;
            rb = GetComponent<Rigidbody>();
            rb.freezeRotation = true;
        }
        else
        {
            transform.Find("Camera").transform.gameObject.SetActive(false);
            transform.Find("Third person camera").transform.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        isGrounded = Physics.CheckSphere(transform.position - Vector3.up, groundDistance, layerGround);
       
        
        PlayerInput();

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            canJump = true;
        }

    }

    void PlayerInput()
    {
        x = Input.GetAxisRaw("Horizontal");
        z = Input.GetAxisRaw("Vertical");

        moveDirection = new Vector3(x, 0, z).normalized;
    }

    private void FixedUpdate()
    {
        if (IsLocalPlayer)
        {
            animator.SetBool("isRunning", false);
            MovePlayer();
            if (canJump)
            {
                Jump();
                canJump = false;
            }
        }
        
    }


    void MovePlayer()
    {
        if (moveDirection.magnitude >= 0.1f)
        {
            // rotation
            float targetAngle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, 0.1f);
            rb.MoveRotation(Quaternion.Euler(0f, angle, 0f));

            // mouvement
            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            rb.MovePosition(transform.position + moveDir * moveSpeed);
            animator.SetBool("isRunning", true);
        }
    }

    void Jump()
    {
        rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }

}

