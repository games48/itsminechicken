using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Messaging;

public class PlayerShoot : NetworkBehaviour
{
    public GameObject bullet;
    public Transform shootOrigin;

    private void Update()
    {
        if (IsLocalPlayer)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                ShootServerRpc();
            }
        }
    }

    // Le client va demander au serveur d'�x�cuter ce code
    [ServerRpc]
    void ShootServerRpc()
    {
        ShootClientRpc();
    }

    // Le serveur va dire � tous les clients d'�x�cuter ce code
    [ClientRpc]
    void ShootClientRpc()
    {
        GameObject go = Instantiate(bullet, shootOrigin.position, Quaternion.identity);
        go.GetComponent<Rigidbody>().AddForce(transform.forward * 50, ForceMode.Impulse);
        Destroy(go, 5f);
    }
}
