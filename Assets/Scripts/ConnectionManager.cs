using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Transports.UNET;
using System;

public class ConnectionManager : MonoBehaviour
{
    public GameObject connectionPannel;
    public string ipAddress = "127.0.0.1";
    UNetTransport transport;

    Vector3 spawn = new Vector3(350, 10, 20);

   public void Host()
    {
        connectionPannel.SetActive(false);
        NetworkManager.Singleton.ConnectionApprovalCallback += ApprovalCheck;
        NetworkManager.Singleton.StartHost(spawn, Quaternion.identity);
    }

    private void ApprovalCheck(byte[] connectionData, ulong clientID, NetworkManager.ConnectionApprovedDelegate callback)
    {
        bool approve = System.Text.Encoding.ASCII.GetString(connectionData) == "Password";
        callback(true, null, approve, spawn, Quaternion.identity);
    }

    public void Join()
    {
        transport = NetworkManager.Singleton.GetComponent<UNetTransport>();
        transport.ConnectAddress = ipAddress;
        connectionPannel.SetActive(false);
        NetworkManager.Singleton.NetworkConfig.ConnectionData = System.Text.Encoding.ASCII.GetBytes("Password");
        NetworkManager.Singleton.StartClient();
    }

    public void IPAddressChange(string newAddress)
    {
        ipAddress = newAddress;
    }

}
